# Arkae


Reference archaeologist sites in Afghanistan.

### Desktop

![desktop](readme/arkae-desktop.gif)

### Mobile

![mobile](readme/arkae-mobile.gif)

### Progressive Web App

![WPA](readme/arkae-wpa.gif)

 
## Build Setup

``` bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn run dev

# build for production and launch server
$ yarn run build
$ yarn start

# generate static project
$ yarn run generate
```

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).
