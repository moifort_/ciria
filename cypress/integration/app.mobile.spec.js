/// <reference types="Cypress" />

context('Application', () => {

  beforeEach(() => {
    cy.viewport(550, 750) // Phone screen
    cy.setCookie('i18n_redirected', 'en') // Avoid lang redirect
  })

  it('Should go directly on search page', () => {
    cy.visit('http://localhost:3000')
    cy.location('pathname').should('include', '/mobile/')
  })

  it('Should witch in desktop mode', () => {
    cy.visit('/mobile/settings')
    cy.get('button[value="settings"]').click()
      .get('#switch-desktop').click()
      .location('pathname').should('not.include', '/mobile/')
  })

})
