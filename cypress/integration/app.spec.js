/// <reference types="Cypress" />

context('Application', () => {

  beforeEach(() => {
    cy.setCookie('i18n_redirected', 'en') // Avoid lang redirect
  })

  it('Should go directly on search page', () => {
    cy.visit('http://localhost:3000')
    cy.location('pathname').should('include', 'search')
  })

  it('Should witch in mobile mode', () => {
    cy.visit('/settings')
    cy.get('#switch-mobile').click()
      .location('pathname').should('include', '/mobile/')
  })

})
