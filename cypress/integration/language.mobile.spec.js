/// <reference types="Cypress" />

context('Language', () => {

  beforeEach(() => {
    cy.viewport(550, 750) // Phone screen
    cy.setCookie('i18n_redirected', 'en') // Avoid lang redirect
  })

  afterEach(() => {
    cy.reload() // Clear storage
  })

  it('Should change language', () => {
    cy.visit('/mobile/')
    cy.get('button[value="settings"]').click()
      .get('#en').click()

    cy.visit('/mobile/')
    cy.get('button[value="settings"]').click()
      .get('#fr').click()
      .location('pathname').should('include', 'fr')

    cy.visit('/mobile/')
    cy.get('button[value="settings"]').click()
      .get('#ar').click({force: true})
      .location('pathname').should('include', 'ar')
  })

})
