/// <reference types="Cypress" />

context('Language', () => {

  beforeEach(() => {
    cy.setCookie('i18n_redirected','en') // Avoid lang redirect
  })

  afterEach(() => {
    cy.reload() // Clear storage
  })

  it('Should change language', () => {
    cy.visit('/settings')
    cy.get('#en').click()

    cy.visit('/settings')
    cy.get('#fr').click()
      .location('pathname').should('include', 'fr')

    cy.visit('/settings')
    cy.get('#ar').click({force: true})
      .location('pathname').should('include', 'ar')
  })

})
