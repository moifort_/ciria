/// <reference types="Cypress" />

context('Map', () => {

  beforeEach(() => {
    cy.viewport(550, 750) // Phone screen
    cy.setCookie('i18n_redirected','en') // Avoid lang redirect
    cy.visit('/mobile/')
  })

  afterEach(() => {
    cy.reload() // Clear store
  })

  it('Should hide default marker', () => {
    cy.get('button[value="settings"]').click()
    cy.get('.default-marker').should('exist')
    cy.get('#marker').click()
    cy.get('.default-marker').should('not.exist')
  })

  it('Should display selected site', () => {
    cy.get('.selected-marker').should('not.exist')
    cy.get('input[type="text"]').type('Lagar Baba',).should('have.value', 'Lagar Baba')
      .type('{enter}')

    cy.get('div[role="listitem"]').click()

    cy.get('.selected-marker').should('exist').and('have.length', 1)
  })

  it('Should display searched site', () => {
    cy.get('.searched-marker').should('not.exist')
    cy.get('input[type="text"]').type('la',).should('have.value', 'la')
      .type('{enter}')

    cy.get('.searched-marker').should('exist').and('have.length', 10)
  })
})
