/// <reference types="Cypress" />

context('Search', () => {

  beforeEach(() => {
    cy.viewport(550, 750) // Phone screen
    cy.setCookie('i18n_redirected','en') // Avoid lang redirect
    cy.visit('/mobile/')
  })

  afterEach(() => {
    cy.reload() // Clear store
  })

  it('Should display 10 results when "la" word is enter', () => {
    cy.get('input[type="text"]').type('la',).should('have.value', 'la')
      .type('{enter}')

    cy.get('.search-list').children('div[role="listitem"]').should('have.length', 10)
  })

  it('Should clear result when click on "Clear search" button', () => {
    cy.get('input[type="text"]')
      .type('la',).should('have.value', 'la')
      .type('{enter}')

    cy.get('.search-list')
      .children('div[role="listitem"]').should('have.length', 10)

    cy.get('button.clear-search').click()
      .get('div[role="listitem"]').should('not.exist')
  })

  it('Should display site when click on "i" button', () => {
    cy.get('input[type="text"]').type('Lagar Baba',).should('have.value', 'Lagar Baba')
      .type('{enter}')

    cy.get('.more').click()
    cy.location('pathname').should('include', 'site/f4d79ce8-7ade-48e2-bd7a-9237b402c6f9')
  })

  it('Should select site when click on the element of the list', () => {
    cy.get('input[type="text"]').type('Lagar Baba',).should('have.value', 'Lagar Baba')
      .type('{enter}')

    cy.get('div[role="listitem"]').click()

    cy.get('.selected').should('be.visible')
  })

})
