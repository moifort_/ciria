/// <reference types="Cypress" />

context('Site', () => {

  beforeEach(() => {
    cy.setCookie('i18n_redirected','en') // Avoid lang redirect
  })

  afterEach(() => {
    cy.reload() // Clear storage
  })

  it('Should display Lagar Baba site', () => {
    cy.visit('/site/f4d79ce8-7ade-48e2-bd7a-9237b402c6f9')
    cy.contains('Lagar Baba')
  })

})
