const functions = require('firebase-functions')
const admin = require('firebase-admin')
const algoliasearch = require('algoliasearch')
const events = require('./events.json')

const ALGOLIA_ID = "J5IAMHJTDK"
const ALGOLIA_ADMIN_KEY = "50da2d524a036fd7768cc66f9a2c6418"
const ALGOLIA_SEARCH_KEY = "bd944241b675d82980226005c503f590"
const search = algoliasearch(ALGOLIA_ID, ALGOLIA_ADMIN_KEY)

const csv = require('csvtojson')
const uuid = require('uuid/v4')
const fs = require('fs')
const Supercluster = require('supercluster')
admin.initializeApp()

const db = admin.firestore()


///////////////////
//// Commandes ////
///////////////////
// exports.createSite = functions.https.onRequest(async (request, response) => {
//   const payload = {
//     id: uuid(),
//     name: ` test + ${uuid()}`,
//   }
//   const event = {
//     type: "site",
//     payloadType: "siteCreated",
//     payload,
//     metadata: {userName: "Thibaut Mottet"},
//     revision: "1.0.0",
//     timestamp: admin.firestore.Timestamp.now(),
//   }
//   await db.collection("events").add(event)
//   return response.sendStatus(200)
// })
//
// exports.updateSiteName = functions.https.onRequest(async (request, response) => {
//   const id = request.query.id
//   if (!id) return response.status(300).send("id is mandatory")
//   const payload = {
//     id,
//     name: `updated + ${uuid()}`,
//   }
//   const event = {
//     type: "site",
//     payloadType: "siteNameUpdated",
//     payload,
//     metadata: {userName: "Thibaut Mottet"},
//     revision: "1.0.0",
//     timestamp: admin.firestore.Timestamp.now(),
//   }
//   await db.collection("events").add(event)
//   return response.sendStatus(200)
// })

///////////////////
///// Counter /////
///////////////////
exports.siteTotalCounter = functions.firestore.document('sites/{siteId}')
  .onWrite(async (change) => await counter(change, db.collection("counters").doc("site")))

exports.eventTotalCounter = functions.firestore.document('events/{eventId}')
  .onWrite(async (change) => await counter(change, db.collection("counters").doc("event")))

async function counter(change, dbReference) {
  if (!change.before.exists) {
    await dbReference.update({total: admin.firestore.FieldValue.increment(1)})

  } else if (!change.after.exists) {
    await dbReference.update({total: admin.firestore.FieldValue.increment(-1)})
  }
  return Promise.resolve()
}

///////////////////
/// Projections ///
///////////////////
exports.onSiteEvent = functions.firestore.document('events/{eventId}')
  .onWrite(async (change) => {
    const event = change.after.data()
    await buildProjection(event)
    return Promise.resolve()
  })

async function buildProjection(event) {
  const {payloadType, payload, type} = event
  if (type !== "site") return Promise.resolve("No site type")

  const sites = db.collection("sites")
  if (payloadType === "siteCreated") {
    const {id, name, description, latitude, longitude} = payload
    const site = {}
    if (name) {
      site.name = name
    }
    if (description) {
      site.description = description
    }
    if (latitude && longitude) {
      site.location = new admin.firestore.GeoPoint(parseFloat(latitude), parseFloat(longitude))
      site.hasLocation = true
    } else {
      site.hasLocation = false
    }
    if (site === {}) {
      return Promise.resolve() // Do nothing
    }
    return sites.doc(id).set(site)
  } else if (payloadType === "siteNameUpdated") {
    const {id, name} = payload
    return sites.doc(id).update({name})
  }
  return Promise.resolve()
}


exports.searchLocation = functions.https.onRequest(async (request, response) => {
  return response.send(`https://mt0.google.com/vt/lyrs=y&hl=en&x=${long2tile(68.7444, 16)}&y=${lat2tile(32.4817, 16)}&z=16`)
})

function long2tile(lon, zoom) {
  return (Math.floor((lon + 180) / 360 * Math.pow(2, zoom)))
}

function lat2tile(lat, zoom) {
  return (Math.floor((1 - Math.log(Math.tan(lat * Math.PI / 180) + 1 / Math.cos(lat * Math.PI / 180)) / Math.PI) / 2 * Math.pow(2, zoom)))
}

///////////////////
////// Tools //////
///////////////////
exports.replay = functions.https.onRequest(async (request, response) => {
  // const events = await db.collection("events").orderBy("timestamp", "asc").get()
  //const events = await db.collection("sites").where("hasLocation", "==", true).get()
  // const sites = events.map(event => {
  //   event.id = uuid()
  //   if (event.payload.latitude && event.payload.longitude) {
  //     event.payload.latitude = parseFloat(event.payload.latitude)
  //     event.payload.longitude = parseFloat(event.payload.longitude)
  //   }
  //   return event
  // })
  //
  // fs.writeFile("events.json", JSON.stringify(sites), function (err) {
  //   if (err) {
  //     console.log(err)
  //   }
  // })
  //events.forEach(async (event) => await buildProjection(event.data()))

  // const eventstile = events.filter(event => event.payload.latitude && event.payload.longitude)
  //   .map(event => {
  //   return {
  //     "id": uuid(),
  //     "payloadType": "tileAdded",
  //     "payload": {
  //       "x": long2tile(event.payload.longitude, 16),
  //       "y": lat2tile(event.payload.latitude, 16),
  //       "zoom": 16
  //     },
  //     "metadata": {
  //       "userName": "Thibaut Mottet"
  //     },
  //     "type": "site",
  //     "aggregateId": event.aggregateId,
  //     "revision": "1.0.0",
  //     "timestamp": Date.now()
  //   }
  // })
  // console.log(eventstile.length)
  //
  // fs.writeFile("events1.json", JSON.stringify(events.concat(eventstile)), function (err) {
  //   if (err) {
  //     console.log(err)
  //   }
  // })

  const sites = db.collection("sites")
  events.filter(event => event.payloadType === "tileAdded")
    .forEach(event => sites.doc(event.aggregateId).update({
      "tile.x": event.payload.x,
      "tile.y": event.payload.y,
      "tile.zoom": event.payload.zoom,
    }))


  return response.sendStatus(200)
})

// {
//   "id": "a159e5f8-ab3b-4469-8016-4c510926f991",
//   "payloadType": "siteCreated",
//   "payload": {
//   "id": "dcb166cc-68f4-4f04-98a7-25ddc97cd9ad",
//     "name": "Abaka",
//     "description": "Three rounded mounds, all dented by rice fields.\nA) rounded mound (30m diam.,  5m high), and second smaller mound, 400m southeast to the first (20m diam.);\nB) Tepe Qursi: oblong mound (50m), flattened on top;\nC) square platform (30x30m)\nOn the trenches west of A), jars in situ on actual floor, containing charcoal, plaster, burnt clay, over an ashy deposit."
// },
//   "metadata": {
//   "userName": "Thibaut Mottet"
// },
//   "type": "site",
//   "aggregateId": "dcb166cc-68f4-4f04-98a7-25ddc97cd9ad",
//   "revision": "1.0.0",
//   "timestamp": 1554124100
// },

// location/{uuid}/
// siteId
exports.cluster = functions.https.onRequest(async (request, response) => {
  this.sites = JSON.parse(fs.readFileSync('sites.json', 'utf8'))
  const sitesGeo = this.sites.map(site => ({
    "siteId": site.id,
    "geometry": {"type": "Point", "coordinates": [site.location._longitude, site.location._latitude]}
  }))
  const clusterable = new Supercluster({extent: 256}).load(sitesGeo)
  const index = search.initIndex("locations")

  // add all point for 14 => 18
  // this.sites.forEach(async site => {
  //   console.log(site.id)
  //   const point = {
  //     objectID: site.id,
  //     type: "point",
  //     zoom: [14, 15, 16, 17, 18],
  //     _geoloc: {
  //       lat: site.location._latitude,
  //       lng: site.location._longitude,
  //     },
  //     siteId: site.id,
  //   }
  //   if (site.name) {
  //     point.siteName = site.name
  //   }
  //   await index.saveObject(point)
  // })

  for (let zoomLevel = 0; zoomLevel < 14; zoomLevel++) {
    const clusters = clusterable.getClusters([-180, -90, 180, 90], zoomLevel)
    clusters.forEach(async cluster => {
      if (cluster.siteId) {
        const myPoint = await index.getObject(cluster.siteId)
        myPoint.zoom.push(zoomLevel)
        await index.partialUpdateObject({
          objectID: cluster.siteId,
          zoom: myPoint.zoom
        })
      } else {
        const longitude = cluster.geometry.coordinates[0]
        const latitude = cluster.geometry.coordinates[1]
        const clusterId = uuid()
        const point = {
          objectID: clusterId,
          type: "cluster",
          zoom: [zoomLevel],
          _geoloc: {
            lat: latitude,
            lng: longitude,
          },
          totalCount: cluster.properties.point_count,
        }
        await index.saveObject(point)
      }
      console.log("cluster id", cluster.id)
    })
  }

  return response.sendStatus(200)
})

// exports.exportCsv = functions.https.onRequest(async (request, response) => {
//   const sites = await csv().fromFile('sites.csv')
//   sites.forEach(async site => {
//     const payload = {id: uuid()}
//     if (site.name !== "NULL") {
//       payload.name = site.name
//     }
//     if (site.description !== "NULL") {
//       payload.description = site.description
//     }
//     if (site.longitude !== "NULL") {
//       payload.longitude = site.longitude
//     }
//     if (site.latitude !== "NULL") {
//       payload.latitude = site.latitude
//     }
//     const event = {
//       type: "site",
//       payloadType: "siteCreated",
//       aggregateId: payload.id,
//       payload,
//       metadata: {userName: "Thibaut Mottet"},
//       revision: "1.0.0",
//       timestamp: admin.firestore.Timestamp.now(),
//     }
//     await db.collection("events").add(event)
//   })
//   return response.sendStatus(200)
// })
