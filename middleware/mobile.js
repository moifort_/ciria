export default function (ctx) {
  const { route, redirect, store } = ctx
  const { path, query } = route
  const forceMode = store.state.application.forceMode
  store.commit('application/isMobile', path.includes('/mobile/'))
  if (window.innerWidth < 700 && !path.includes('/mobile/') && !forceMode) {
    return redirect('/mobile/')
  }
}


