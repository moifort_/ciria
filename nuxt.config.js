import pkg from './package'

export default {
  mode: 'spa',

  /*
  ** Headers of the page
  */
  head: {
    title: pkg.name,
    meta: [
      {charset: 'utf-8'},
      {name: 'viewport', content: 'width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=0'},
      {hid: 'description', name: 'description', content: pkg.description}
    ],
    link: [
      {rel: 'icon', type: 'image/png', href: '/favicon.png'},
    ],
  },

  /*
  ** Customize the progress-bar color
  */
  loading: {color: '#fff'},

  /*
  ** Global CSS
  */
  css: [],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '@/plugins/firebase',
    '@/plugins/algolia',
    '@/plugins/leaflet',
    '@/plugins/filters',
    '@/plugins/font-awesome',
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    '@nuxtjs/axios', // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/pwa',
    '@nuxtjs/vuetify',
    ['nuxt-i18n', {
      locales: [
        {code: 'en', name: 'English', iso: 'en-US'},
        {code: 'fr', name: 'Français', iso: 'fr-FR'},
        {code: 'ar', name: 'عربى', iso: 'ar'},
      ],
      defaultLocale: 'en',
      vueI18nLoader: true,
      rootRedirect: 'search',
    }],
    ['@nuxtjs/google-analytics', {
      id: 'UA-42286365-7',
      autoTracking: {exception: true},
      debug: {
        sendHitTask: process.env.NODE_ENV === 'PROD'
      },
    }]
  ],

  router: {
    middleware: 'mobile'
  },

  manifest: {
    lang: 'fr',
    start_url: '/mobile/',
    description: "ArchaeoGeek",
  },


  /*
  ** Axios module configuration
  */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
  },

  env: {
    firebase: {
      apiKey: process.env.FIREBASE_API_KEY_ARKAE,
      authDomain: "ciriaco-b93c3.firebaseapp.com",
      projectId: "ciriaco-b93c3",
      databaseURL: "https://ciriaco-b93c3.firebaseio.com",
      storageBucket: "ciriaco-b93c3.appspot.com",
      messagingSenderId: "189102703869"
    },
  },

  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
      config.module.rules.push({
        resourceQuery: /blockType=i18n/,
        type: "javascript/auto",
        loader: ["@kazupon/vue-i18n-loader", "yaml-loader"],
      })
    }
  },
}
