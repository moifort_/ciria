import Vue from 'vue'
import algoliasearch from 'algoliasearch'

const algoliaPlugin = {
  install(Vue) {
    if (Vue.__nuxt_algolia_installed__) {
      return
    }
    Vue.__nuxt_algolia_installed__ = true
    if (!Vue.prototype.$algolia) {
      Vue.prototype.$algolia = algoliasearch("J5IAMHJTDK", "bd944241b675d82980226005c503f590")
    }
  }
}

Vue.use(algoliaPlugin)

export default (ctx) => {
  const {app, store} = ctx
  app.$algolia = Vue.prototype.$algolia
  ctx.$algolia = Vue.prototype.$algolia
  store.$algolia = Vue.prototype.$algolia
}
