import Vue from 'vue'
import {LMap, LTileLayer, LMarker, LPopup, LIcon} from 'vue2-leaflet'
import {Icon} from 'leaflet'
import 'leaflet-active-area'
import 'leaflet/dist/leaflet.css'


delete Icon.Default.prototype._getIconUrl

Icon.Default.mergeOptions({
  iconRetinaUrl: require('~/assets/marker-x2.png'),
  iconUrl: require('~/assets/marker.png'),
  shadowUrl: '',
  iconSize: [28, 35],
  iconAnchor: [14, 35]
})


Vue.component('l-map', LMap)
Vue.component('l-tile-layer', LTileLayer)
Vue.component('l-marker', LMarker)
Vue.component('l-popup', LPopup)
Vue.component('l-icon', LIcon)

