export const state = () => ({
  isMobile: false,
  forceMode: false,
})

export const mutations = {
  isMobile: (state, isMobile) => state.isMobile = isMobile,
  forceMode: (state, forceMode) => state.forceMode = forceMode,
}
