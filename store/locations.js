export const state = () => ({
  searched: [],
})

export const actions = {
  search: async function ({commit}, {topLeft, bottomRight, zoom}) {
    const topLeftLat = Math.max(Math.min(topLeft.lat, 90), -90)
    const topLeftLng = Math.max(Math.min(topLeft.lng, 180), -180)
    const bottomRightLat = Math.max(Math.min(bottomRight.lat, 90), -90)
    const bottomRightLng = Math.max(Math.min(bottomRight.lng, 180), -180)
    const response = await this.$algolia.initIndex("locations")
      .search("", {
        "hitsPerPage": 200,
        "insideBoundingBox": `${topLeftLat}, ${topLeftLng}, ${bottomRightLat}, ${bottomRightLng}`,
        "filters": `zoom:${zoom}`,
      })

    const sites = response.hits.map(site => ({
      id: site.objectID,
      type: site.type,
      location: {
        latitude: site._geoloc.lat,
        longitude: site._geoloc.lng,
      },
      totalCount: site.totalCount,
      name: site.siteName ? site.siteName : `Site ${site.objectID}`,
    }))
    return commit('search', sites)
  }
}

export const mutations = {
  search: (state, sites) => state.searched = [...sites],
}
