export const state = () => ({
  displayMarker: true,
})


export const mutations = {
  displayMarker: (state, displayMarker) => state.displayMarker = displayMarker,
}
