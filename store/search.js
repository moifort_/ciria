export const state = () => ({
  isLoading: false,
  selected: {},
  searched: [],
  status: 'INIT',
})

export const actions = {
  search: async function ({commit}, text) {
    commit('isLoading', true)
    commit('status', 'SEARCHING')
    try {
      if (!text || text.trim().length === 0) {
        return commit('search', [])
      }
      const response = await this.$algolia.initIndex("locations").search(text, {"hitsPerPage": 10,})

      const sites = response.hits.map(site => ({
        id: site.objectID,
        type: site.type,
        location: {
          latitude: site._geoloc.lat,
          longitude: site._geoloc.lng,
        },
        totalCount: site.totalCount,
        name: site.siteName ? site.siteName : `Site ${site.objectID}`,
      }))
      commit('search', sites)
      commit('status', sites.length === 0 ? 'NO_RESULT' : 'DONE')
    } catch (e) {
      commit('status', 'ERROR')
    } finally {
      commit('isLoading', false)
    }
  },
  select: async function ({commit}, site) {
    return commit('select', site)
  },
}

export const mutations = {
  select: (state, site) => state.selected = {...site},
  search: (state, sites) => state.searched = [...sites],
  isLoading: (state, isLoading) => state.isLoading = isLoading,
  status: (state, status) => state.status = status,
}
