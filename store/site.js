export const state = () => ({
  site: {},
  isLoading: false,
  status:'INIT',
})

export const actions = {
  get: async function ({commit, state}, id) {
    commit('isLoading', true)
    commit('status', 'SEARCHING')
    try {
      if (!id) return commit('site', {})
      if (id === state.site.id) return
      const snapshot = await this.$firebase.firestore().collection("sites").doc(id).get()
      return commit('site', {id: snapshot.id, ...snapshot.data()})
    } catch (e) {
      commit('status', 'ERROR')
    } finally {
      commit('isLoading', false)
    }
  },
}

export const mutations = {
  site: (state, site) => state.site = {...site},
  isLoading:  (state, isLoading) => state.isLoading = isLoading,
  status: (state, status) => state.status = status,
}
